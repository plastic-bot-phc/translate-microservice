import { SupportedLanguage } from '@enums/supported-language';

export class TranslateDTO {
  text: string;
  language: SupportedLanguage;
}

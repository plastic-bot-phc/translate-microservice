import * as translate from '@asmagin/google-translate-api';
import { SupportedLanguage } from '@enums/supported-language';
import { Injectable } from '@nestjs/common';

@Injectable()
export class TranslatorService {
  async translate(text: string, language: SupportedLanguage): Promise<string> {
    if (text.length > 2500) {
      return this.translateLong(text, language);
    }

    const result = await translate(text, { to: language });
    return result.text;
  }

  async translateLong(
    text: string,
    language: SupportedLanguage,
  ): Promise<string> {
    const words: string[] = text.split(' ');
    const chunks: string[] = [];

    let currentText: string = '';

    for (const word of words) {
      const possibleText: string = !currentText
        ? word
        : currentText + ' ' + word;

      if (possibleText.length > 500) {
        chunks.push(currentText);
        currentText = '';
      } else {
        currentText = possibleText;
      }
    }

    const results = await Promise.all(
      chunks.map((chunk) => translate(chunk, { to: language })),
    );

    return results.map((result) => result.text).join(' ');
  }
}

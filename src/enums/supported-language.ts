export enum SupportedLanguage {
  SPANISH = 'es',
  ENGLISH = 'en',
  PORTUGUESE = 'pt',
}

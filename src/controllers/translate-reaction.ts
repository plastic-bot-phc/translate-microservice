import { TRANSLATE_PATTERN } from '@constants/pattern';
import { TranslateDTO } from '@dtos/translate';
import { Controller } from '@nestjs/common';
import { MessagePattern, Payload } from '@nestjs/microservices';
import { TranslatorService } from '@services/translator';

@Controller()
export class TranslateReactionController {
  constructor(private readonly translatorService: TranslatorService) {}

  @MessagePattern(TRANSLATE_PATTERN)
  async translate(@Payload() data: TranslateDTO): Promise<string> {
    return this.translatorService.translate(
      data.text.replace(/\s+/g, ' ').trim(),
      data.language,
    );
  }
}

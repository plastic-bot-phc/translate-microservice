import { TranslateReactionController } from '@controllers/translate-reaction';
import { Module } from '@nestjs/common';
import { ConfigModule } from '@nestjs/config';
import { TranslatorService } from '@services/translator';

@Module({
  imports: [ConfigModule.forRoot()],
  controllers: [TranslateReactionController],
  providers: [TranslatorService],
})
export class AppModule {}
